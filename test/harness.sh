#!/bin/sh
set -eu

BUILDDIR=$(pwd)
export PATH="$BUILDDIR:$PATH"

TMPDIR="."
TMPDIR=$TMPDIR/pp-tests
mkdir -p "$TMPDIR"

setup() {
    printf "[----------] Test environment set-up.\n"
}

teardown() {
    rm -rf "$TMPDIR"
    printf "[----------] Test environment teardown.\n"
}

trap teardown EXIT

src=$TMPDIR/src
dst=$TMPDIR/dst
res=$TMPDIR/res

npass=0
nfail=0

run_tests() {
    printf "[==========] Running %d test cases.\n" "$#"

    for t in "$@"
    do
        printf "[ %-8s ] %s\n" "RUN" "$t"

        if "$t"
        then
            printf "[ %8s ] %s\n" "OK" "$t"
            npass=$((npass+1))
        else
            printf "[ %8s ] %s\n" "NOK" "$t"
            nfail=$((nfail+1))
        fi
    done
    
    printf "[==========] %d test cases ran.\n" "$#"
    printf "[  PASSED  ] %d tests.\n" $npass
    printf "[  FAILED  ] %d tests.\n" $nfail

    if [ $nfail -ne 0 ]
    then
        exit 1
    fi
}

empty_source() {
    touch $src
    echo "dst" > $dst
    cat $dst > $res

    pp $src $dst

    if cmp -s $res $dst
    then
        return 0
    else
        return 1
    fi
}

empty_destination() {
    echo "src" > $src
    printf "" > $dst

    pp $src $dst

    if cmp -s $dst $src
    then
        return 0
    else
        return 1
    fi
}

trivial_test() {
    echo "src" > $src
    echo "dst" > $dst
    cat $src $dst > $res

    pp $src $dst

    if cmp -s $res $dst
    then
        return 0
    else
        return 1
    fi
}

stdin_source() {
    echo "dst" > $dst
    echo "This is from stdin" | cat - $dst > $res

    echo "This is from stdin" | pp - $dst

    if cmp -s $res $dst
    then
        return 0
    else
        return 1
    fi
}

utf8_source() {
    printf 'Ώ¥あ∀𝜔\n∊ℝ 𝜉(\n𝜔)≿⌨' > $src
    printf "" > $dst

    pp $src $dst

    if cmp -s $src $dst
    then
        return 0
    else
        return 1
    fi
}

filename_dash() {
    dash_src=-src
    echo "Source file with dash" > $dash_src
    printf "" > $dst

    timeout --preserve-status 1 pp $dash_src $dst

    if [ $? -gt 0 ]
    then
        return 1
    fi

    ret=1
    if cmp -s -- $dash_src $dst
    then
        ret=0
    fi

    rm -- $dash_src
    return $ret
}

opt_source() {
    echo "dst" > $dst
    echo "This is from stdin" | cat - $dst > $res

    echo "This is from stdin" | pp $dst

    if cmp -s $res $dst
    then
        return 0
    else
        return 1
    fi
}

setup
run_tests \
    empty_source \
    empty_destination \
    trivial_test \
    stdin_source \
    utf8_source \
    filename_dash \
    opt_source
