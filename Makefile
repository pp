.POSIX:

.PHONY: all test clean install uninstall

include config.mk

SRCS != echo src/*.c
OBJS = ${SRCS:.c=.o}

all: ${PROG}

${PROG}: ${OBJS}
	${CC} ${LDFLAGS} -o $@ ${OBJS} ${LDLIBS}

${PROG}.o: ${PROG}.c

test: ${PROG}
	./test/harness.sh

clean:
	${RM} -f ${OBJS} ${PROG}

install: ${PROG} ${PROG}.1
	mkdir -p ${DESTDIR}${PREFIX}/bin
	install -m 755 ${PROG} ${DESTDIR}${PREFIX}/bin
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	gzip < ${PROG}.1 > ${DESTDIR}${MANPREFIX}/man1/${PROG}.1.gz

uninstall:
	${RM} -f ${DESTDIR}${PREFIX}/bin/${PROG}
	${RM} -f ${DESTDIR}${MANPREFIX}/man1/${PROG}.1.gz

.c.o:
	$(CC) -c $(CFLAGS) -o $@ $<
