/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2021 Alessio Chiapperini
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#define TMP_FILE "tmp_dst"

static void
usage(void)
{
	(void)fprintf(stderr, "usage: pp source target\n");
}

static int
pp(const char *src, const char *dst)
{
	int ret = 0;

	FILE *src_fp = stdin;
	if (src[0] != '-' || src[1] != '\0') {
		src_fp = fopen(src, "r");
		if (src_fp == 0) {
			ret = 1;
			perror("pp");
			goto src_open_err;
		}
	}

	FILE *dst_fp = fopen(dst, "r");
	if (dst_fp == 0) {
		ret = 1;
		perror("pp");
		goto dst_open_err;
	}

	FILE *tmp_fp = fopen(TMP_FILE, "w");
	if (tmp_fp == 0) {
		ret = 1;
		perror("pp");
		goto tmp_open_err;
	}

	char buf[BUFSIZ];
	while (fgets(buf, BUFSIZ, src_fp) != 0) {
		(void)fprintf(tmp_fp, "%s", buf);
	}
	while (fgets(buf, BUFSIZ, dst_fp) != 0) {
		(void)fprintf(tmp_fp, "%s", buf);
	}

	(void)fclose(tmp_fp);
	if (remove(dst) == -1) {
		ret = 1;
		perror("pp");
	}

	if (rename(TMP_FILE, dst) == -1) {
		ret = 1;
		perror("pp");
	}

tmp_open_err:
	(void)fclose(dst_fp);
dst_open_err:
	(void)fclose(src_fp);
src_open_err:
	return (ret);
}

int
main(int argc, char *argv[])
{
	char *src = "-";
	char *dst = 0;

	if (argc > 3) {
		errno = E2BIG;
		perror("pp");
		return (EXIT_FAILURE);
	} else if (argc == 2) {
		dst = argv[1];
	} else if (argc < 2) {
		usage();
		return (EXIT_FAILURE);
	} else {
		src = argv[1];
		dst = argv[2];
	}

	if (pp(src, dst) == 1) {
		return (EXIT_FAILURE);
	}

	return (EXIT_SUCCESS);
}
