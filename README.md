# pp (prepend)

The **pp** (prepend) utility takes the content of the file named by the *source* operand and inserts or prepends it
above the first line of the file named by the *target* operand. The file operands are processed in command-line order.
If the *source* file is a single dash (‘-’) or absent, **pp** reads from the standard input.

So, essentially, if I have a CSV file without a header and I want to quickly insert the header from a file, I
would run:

```sh
$ pp header.txt spreadsheet.csv
```

and it would be functionally equivalent to

```sh
$ cat header.txt spreadsheet.csv > temp
$ mv temp spreadsheet.csv
```

## Features
* Quality
    * Compiled with security hardening flags
    * Static analysis integrated using clang's `scan-build` using checkers `alpha.security`, `alpha.core.CastSize`,
    `alpha.core.CastToStruct`, `alpha.core.IdenticalExpr`, `alpha.core.PointerArithm`, `alpha.core.PointerSub`,
    `alpha.core.SizeofPtr`, `alpha.core.TestAfterDivZero`, `alpha.unix`
    * Test harness
    * Follows [FreeBSD coding style](https://www.freebsd.org/cgi/man.cgi?query=style&sektion=9)
* Portable
    * C99 compliant
    * Self-contained, no external dependencies
    * Easy to compile and uses POSIX make

## Build dependencies
The only dependency is a toolchain supporting the following flags:

```
CFLAGS = -std=c99 -O2 -Wall -Wextra -Wpedantic \
	-Walloca -Wcast-qual -Wconversion -Wformat=2 -Wformat-security \
	-Wnull-dereference -Wstack-protector -Wvla -Warray-bounds \
	-Wbad-function-cast -Wconversion -Wshadow -Wstrict-overflow=4 -Wundef \
	-Wstrict-prototypes -Wswitch-default -Wfloat-equal -Wimplicit-fallthrough \
	-Wpointer-arith -Wswitch-enum \
	-D_FORTIFY_SOURCE=2 \
	-fstack-protector-strong -fPIE -fstack-clash-protection

LDFLAGS = -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack -Wl,-z,separate-code
```

Otherwise you can just remove the security flags and compile it with
```
CFLAGS = -std=c99 -O2 -Wall -Wextra -Wpedantic
LDFLAGS =
```

or pass your own flags to make
```sh
make CC=gcc CFLAGS=... LDFLAGS=...
```

## Installation
Clone this repository then

```sh
$ make PREFIX=/usr install
```

This will install the compiled binary under `PREFIX` (`/usr/bin`) in this case, if not specified `PREFIX` will default
to `/usr/local`. For staged installs, `DESTDIR` is also supported. As the binary does not have any dependency it does
not have to be installed before use.

## Usage
Using **pp** is quite simple, you just specify the *source* file and *target* file
```sh
$ pp source target
```
The source file however, can be omitted, in this case the program takes the input from the standard input until `EOF` or
`^D` is reached
```sh
$ pp target
```
this behavior can also be achieved by using ‘-’ as *source* file
```sh
$ pp - target
```

## Compilation
In order to build on any Unix-like system, simply run `make`. Since the binary does not have any dependencies, it can
just be copied into your `PATH`.

### Test suite
The test suite consists of a POSIX shell script called `harness.sh` contained in the `test` folder. It's output is
similar to [googletest](https://github.com/google/googletest)'s and it can be invoked with `make test` which, if
everything is working should output something similar to
```
./test/harness.sh
[----------] Test environment set-up.
[==========] Running 7 test cases.
[ RUN      ] empty_source
[       OK ] empty_source
[ RUN      ] empty_destination
[       OK ] empty_destination
[ RUN      ] trivial_test
[       OK ] trivial_test
[ RUN      ] stdin_source
[       OK ] stdin_source
[ RUN      ] utf8_source
[       OK ] utf8_source
[ RUN      ] filename_dash
[       OK ] filename_dash
[ RUN      ] opt_source
[       OK ] opt_source
[==========] 7 test cases ran.
[  PASSED  ] 7 tests.
[  FAILED  ] 0 tests.
[----------] Test environment teardown.
```

### Static analysis
Static analysis on the code base is done by using clang's static analyzer run through `scan-build.sh` which wraps the
`scan-build` utility. The checkers used are part of the
[Experimental Checkers](https://releases.llvm.org/12.0.0/tools/clang/docs/analyzer/checkers.html#alpha-checkers)
(aka *alpha* checkers):

* `alpha.security`
* `alpha.core.CastSize`
* `alpha.core.CastToStruct`
* `alpha.core.IdenticalExpr`
* `alpha.core.PointerArithm`
* `alpha.core.PointerSub`
* `alpha.core.SizeofPtr`
* `alpha.core.TestAfterDivZero`
* `alpha.unix`

## Contributing
Send patches on the [mailing list](https://www.freelists.org/list/pp-dev), report bugs on the [issue tracker](https://todo.sr.ht/~spidernet/pp). 

## License
BSD 2-Clause FreeBSD License, see LICENSE.
